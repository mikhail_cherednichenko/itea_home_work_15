// 1. Створити об'єкт "Документ", де визначити властивості "Заголовок, тіло, футер, дата".

const doc = {
    header: "заголовок",
    body: "тіло",
    footer: "футер",
    date: "дата",
};

// 2. Створити вкладений об'єкт - "Додаток".
doc.addition = new Object();

// 3. Створити об'єкт "Додаток",  зі вкладеними об'єктами, "Заголовок, тіло, футер, дата".
doc.addition.header = {
    one: "один",
    two: "два",
};
doc.addition.body = {
    one: "один",
    two: "два",
};
doc.addition.footer = new Object();
doc.addition.date = new Object();

// 4. Створити методи для заповнення та відображення документа. використовуючі оператор in

doc.fill = function (property, value) {
    this[property] = value;
};

doc.fill("header", "підзаголовок");
doc.fill("age", 10);

doc.show = function () {
    
    console.log(`Об'єкт містить:`);

    // цикл заглиблення у вкладені об'єкти
    let deepObj = function (object, path = '') {
        for (let property in object) {
            let temp = `${path}${property}: `;
            showProp(object[property], temp);
        };
    };
    
    // цикл відображення властивостей
    let showProp = function (object, path = '') {
        if (Object.keys(object).length === 0) {
            console.log(`${path}--empty--`);
        } else {
            for (let property in object) {
                let temp = `${path}${property}: `;
                if (typeof object[property] === "object") {
                    deepObj(object[property], temp);
                } else if (typeof object[property] === "function") {
                    console.log(`${temp}--function--`);
                } else {
                    console.log(`${temp}${object[property]}`);
                };
            };
        };
    };

    showProp(this);
};

doc.show();